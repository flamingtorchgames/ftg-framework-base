﻿using System.Collections.Generic;
using UnityEngine;

namespace FlamingTorchGames
{
    namespace FrameworkBase
    {
        /// <summary>
        /// Stores all components related to a game
        /// </summary>
        public class GameComponentsInfoBase
        {
            protected List<object> containedObjects = new List<object>();

            public GameComponentsInfoBase(WorkspaceManagerBase workspace)
            {
                FetchComponentsForGame(workspace, workspace.ActiveGameName);
            }

            public GameComponentsInfoBase(WorkspaceManagerBase workspace, string gameName)
            {
                FetchComponentsForGame(workspace, gameName);
            }

            public string GameName
            {
                get;
                private set;
            }

            public virtual bool IsValid
            {
                get
                {
                    return false;
                }
            }

            public int Count
            {
                get
                {
                    return containedObjects.Count;
                }
            }

            public T ObjectAtIndex<T>(int index)
            {
                if(index < 0 || index >= containedObjects.Count)
                {
                    return default(T);
                }

                return (T)containedObjects[index];
            }

            public virtual string Export()
            {
                return "{}";
            }

            public virtual bool Import(string JSONString)
            {
                return false;
            }

            protected virtual void FetchComponentsForGame(WorkspaceManagerBase workspace, string gameName)
            {
                GameName = gameName;
            }

            protected void AddComponentForGame<T>(string gameName, string assetName) where T : ScriptableObject
            {
                var component = WorkspaceManagerBase.GetGameWorkspaceAsset<T>(gameName, assetName) as ScriptableObject;

                if(component != null)
                {
                    containedObjects.Add(component);
                }
            }
        }
    }
}