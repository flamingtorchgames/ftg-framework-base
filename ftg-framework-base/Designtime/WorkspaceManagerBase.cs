﻿using FlamingTorchGames.CommonScripts;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.IO;
using System.Collections.Generic;

namespace FlamingTorchGames
{
    namespace FrameworkBase
    {
        /// <summary>
        /// This class is responsible for managing access to a game's components
        /// When subclassing this, you MUST override editedGameEditorPrefKey, workspaceFolderName
        /// </summary>
        public class WorkspaceManagerBase
        {
            protected string workspaceFolderName = "Workspaces/Resources/";
            protected string editedGameEditorPrefKey = "FTG_FWB_EDITED_GAME";

            /// <summary>
            /// Gets the currently active game name. If a game is being edited and we're in the editor, it'll override that name if the active game is invalid
            /// </summary>
            public string ActiveGameName
            {
                get
                {
                    string gameName = currentlyActiveGame;

#if UNITY_EDITOR
                    if (gameName == null)
                    {
                        gameName = CurrentlyEditedGame;
                    }
#endif

                    return gameName;
                }
            }

            /// <summary>
            /// Stores the name of the currently active game
            /// </summary>
            public string currentlyActiveGame;

            /// <summary>
            /// Gets an asset from a game's workspace
            /// </summary>
            /// <typeparam name="T">The type of the asset, which must be a ScriptableObject subtype</typeparam>
            /// <param name="name">The name of the game</param>
            /// <param name="assetName">The name of the asset</param>
            /// <returns>The asset, or null</returns>
            public static T GetGameWorkspaceAsset<T>(string name, string assetName) where T : ScriptableObject
            {
                T outAsset = Resources.Load<T>(name + "/" + assetName);

                if (outAsset == null)
                {
                    Debug.LogWarning("Unable to fetch asset with name '" + assetName + "' for game '" + name + "'");
                }

                return outAsset;
            }

#if UNITY_EDITOR
            /// <summary>
            /// gets the name of the game that is currently being edited
            /// </summary>
            public string CurrentlyEditedGame
            {
                get
                {
                    return EditorPrefs.GetString(editedGameEditorPrefKey);
                }

                set
                {
                    EditorPrefs.SetString(editedGameEditorPrefKey, value);
                }
            }

            /// <summary>
            /// Gets the workspace directory
            /// </summary>
            /// <returns>The workspace's directory</returns>
            public string WorkspaceDirectory()
            {
                return Application.dataPath + "/" + workspaceFolderName;
            }

            /// <summary>
            /// Gets the workspace directory for a particular game
            /// </summary>
            /// <param name="name">The name of the game</param>
            /// <returns>The workspace directory for that game</returns>
            public string GameWorkspaceDirectory(string name)
            {
                return WorkspaceDirectory() + "/" + name;
            }

            /// <summary>
            /// Gets the workspace directory local to Unity
            /// </summary>
            /// <returns>The workspace directory</returns>
            public string WorkspaceDirectoryLocal()
            {
                return "Assets/" + workspaceFolderName;
            }

            /// <summary>
            /// Gets the workspace directory local to Unity for a particular game
            /// </summary>
            /// <param name="name">The name of the game</param>
            /// <returns>The local workspace directory for that game</returns>
            public string GameWorkspaceDirectoryLocal(string name)
            {
                return WorkspaceDirectoryLocal() + "/" + name;
            }

            /// <summary>
            /// Gets the workspace directory for resources loading in Unity for a particular game
            /// </summary>
            /// <param name="name">The name of the game</param>
            /// <returns>The local workspace directory for that game</returns>
            public string GameWorkspaceResourcesDirectory(string name)
            {
                return workspaceFolderName + "/" + name + "/";
            }

            /// <summary>
            /// Gets a list of all games
            /// </summary>
            /// <returns>The list of all valid available games</returns>
            public List<string> GetGamesList()
            {
                List<string> availableGames = new List<string>();

                if (Directory.Exists(WorkspaceDirectory()))
                {
                    string[] directories = Directory.GetDirectories(WorkspaceDirectory());

                    foreach (string directoryName in directories)
                    {
                        string actualDirectoryName = Path.GetFileName(directoryName);

                        if (IsGameWorkspaceValid(actualDirectoryName))
                        {
                            availableGames.Add(actualDirectoryName);
                        }
                    }
                }

                return availableGames;
            }

            /// <summary>
            /// Creates a ScriptableObject asset for a game
            /// </summary>
            /// <typeparam name="T">A ScriptableObject type</typeparam>
            /// <param name="gameName">The name of the game</param>
            /// <param name="assetName">The name of the asset</param>
            /// <returns>True if successful, or false</returns>
            protected bool CreateAssetForGame<T>(string gameName, string assetName) where T : ScriptableObject
            {
                return AssetUtilities.CreateScriptableObjectAsset<T>(GameWorkspaceDirectoryLocal(gameName), assetName);
            }

            /// <summary>
            /// Checks whether we have a workspace
            /// </summary>
            /// <returns>True if we have a valid workspace, false otherwise</returns>
            public bool HasWorkspace()
            {
                return Directory.Exists(Application.dataPath + "/" + workspaceFolderName);
            }

            /// <summary>
            /// Checks whether a game workspace is valid
            /// </summary>
            /// <param name="name">The game's name</param>
            /// <returns>True if we have a valid workspace, false otherwise</returns>
            public virtual bool IsGameWorkspaceValid(string name)
            {
                if (!Directory.Exists(GameWorkspaceDirectory(name)))
                {
                    return false;
                }

                return true;
            }

            /// <summary>
            /// Creates a directory at a specific path
            /// </summary>
            /// <param name="path">The path to build the directory</param>
            /// <returns>True if we created the directory or it already existed, false otherwise</returns>
            protected bool CreateDirectoryAtPath(string path)
            {
                try
                {
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                }
                catch (System.Exception e)
                {
                    Debug.LogError("Failed to create directory '" + path + "': " + e);

                    return false;
                }

                return true;
            }

            /// <summary>
            /// Creates a game workspace
            /// </summary>
            /// <param name="name">The name of the game</param>
            /// <returns>True if we created the game's workspace, false otherwise</returns>
            public virtual bool CreateGameInWorkspace(string name)
            {
                if (!HasWorkspace() && !CreateWorkspace())
                {
                    return false;
                }

                if (!CreateDirectoryAtPath(GameWorkspaceDirectory(name)))
                {
                    Debug.LogError("Failed to create game workspace for '" + name + "'");

                    return false;
                }

                return true;
            }

            /// <summary>
            /// Creates the main workspace
            /// </summary>
            /// <returns>True if we created the workspace, false otherwise</returns>
            public virtual bool CreateWorkspace()
            {
                return CreateDirectoryAtPath(Application.dataPath + "/" + workspaceFolderName);
            }
#endif
        }
    }
}
