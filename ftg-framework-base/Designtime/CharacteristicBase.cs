﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace FlamingTorchGames
{
    namespace FrameworkBase
    {
        /// <summary>
        /// This stores the detail for a game characteristic
        /// </summary>
        /// <remarks>Strings are encoded as URL Encoding, we need a better way of encoding them that also takes into account newlines so the JSON parse won't break</remarks>
        [Serializable]
        public class CharacteristicBase
        {
            public const string BoolDataTypeName = "Bool";
            public const string IntDataTypeName = "Int";
            public const string FloatDataTypeName = "Float";
            public const string StringDataTypeName = "String";

            /// <summary>
            /// This is the data types list that identifies the ID of a data type by element index.
            /// </summary>
            /// <remarks>If you subclass this class, you should add in new types and never remove or alter the other types. Otherwise, it WILL break.</remarks>
            [NonSerialized]
            protected List<string> dataTypesIndexes = new List<string> {
                BoolDataTypeName, 
                IntDataTypeName, 
                FloatDataTypeName, 
                StringDataTypeName, 
            };

            /// <summary>
            /// The characteristic's name
            /// </summary>
            public string name;

            /// <summary>
            /// The characteristic's comment
            /// </summary>
            public string comment;

            /// <summary>
            /// The characteristic's data type
            /// </summary>
            public byte dataType;

            /// <summary>
            /// Whether this characteristic requires a valid value
            /// </summary>
            public bool required = false;

            /// <summary>
            /// The characteristic's bool value (valid if dataType is Bool)
            /// </summary>
            public bool boolValue;

            /// <summary>
            /// The characteristic's int value (valid if dataType is Int)
            /// </summary>
            public int intValue;

            /// <summary>
            /// The characteristic's float value (valid if dataType is Float)
            /// </summary>
            public float floatValue;

            /// <summary>
            /// The characteristic's string value (valid if dataType is String)
            /// </summary>
            public string stringValue;

            /// <summary>
            /// Checks whether this characteristic has a valid value
            /// </summary>
            public virtual bool IsValid
            {
                get
                {
                    if (dataType == dataTypesIndexes.IndexOf(BoolDataTypeName) || dataType == dataTypesIndexes.IndexOf(FloatDataTypeName) ||
                        dataType == dataTypesIndexes.IndexOf(IntDataTypeName))
                    {
                        return true;
                    }
                    else if (dataType == dataTypesIndexes.IndexOf(StringDataTypeName))
                    {
                        return stringValue != null;
                    }

                    return false;
                }
            }

            /// <summary>
            /// Gets and Sets the value object in this characteristic
            /// </summary>
            public virtual object Value
            {
                get
                {
                    if(dataType == dataTypesIndexes.IndexOf(BoolDataTypeName))
                    {
                        return boolValue;
                    }
                    else if(dataType == dataTypesIndexes.IndexOf(FloatDataTypeName))
                    {
                        return floatValue;
                    }
                    else if(dataType == dataTypesIndexes.IndexOf(IntDataTypeName))
                    {
                        return intValue;
                    }
                    else if(dataType == dataTypesIndexes.IndexOf(StringDataTypeName))
                    {
                        return stringValue;
                    }

                    return null;
                }

                set
                {
                    if(dataType == dataTypesIndexes.IndexOf(BoolDataTypeName))
                    {
                        if (value.GetType() == typeof(bool))
                        {
                            boolValue = (bool)value;
                        }
                        else
                        {
                            Debug.Log(string.Format("Unknown type assigning to {0} property '{1}': {2}", BoolDataTypeName, name, value.GetType().Name));
                        }
                    }
                    else if(dataType == dataTypesIndexes.IndexOf(FloatDataTypeName))
                    {
                        if (value.GetType() == typeof(float))
                        {
                            floatValue = (float)value;
                        }
                        else if (value.GetType() == typeof(double))
                        {
                            floatValue = (float)((double)value);
                        }
                        else if (value.GetType() == typeof(int))
                        {
                            floatValue = (int)value;
                        }
                        else
                        {
                            Debug.Log(string.Format("Unknown type assigning to {0} property '{1}': {2}", FloatDataTypeName, name, value.GetType().Name));
                        }
                    }
                    else if(dataType == dataTypesIndexes.IndexOf(IntDataTypeName))
                    {
                        if (value.GetType() == typeof(float))
                        {
                            intValue = (int)((float)value);
                        }
                        else if (value.GetType() == typeof(double))
                        {
                            intValue = (int)((double)value);
                        }
                        else if (value.GetType() == typeof(int))
                        {
                            intValue = (int)value;
                        }
                        else
                        {
                            Debug.Log(string.Format("Unknown type assigning to {0} property '{1}': {2}", IntDataTypeName, name, value.GetType().Name));
                        }
                    }
                    else if(dataType == dataTypesIndexes.IndexOf(StringDataTypeName))
                    {
                        if (value.GetType() == typeof(string))
                        {
                            stringValue = (string)value;
                        }
                        else
                        {
                            Debug.Log(string.Format("Unknown type assigning to {0} property '{1}': {2}", StringDataTypeName, name, value.GetType().Name));
                        }
                    }
                }
            }

            public bool IsBool
            {
                get
                {
                    return dataType == dataTypesIndexes.IndexOf(BoolDataTypeName);
                }
            }

            public bool IsFloat
            {
                get
                {
                    return dataType == dataTypesIndexes.IndexOf(FloatDataTypeName);
                }
            }

            public bool IsInt
            {
                get
                {
                    return dataType == dataTypesIndexes.IndexOf(IntDataTypeName);
                }
            }

            public bool IsString
            {
                get
                {
                    return dataType == dataTypesIndexes.IndexOf(StringDataTypeName);
                }
            }

            /// <summary>
            /// Make a copy of this characteristic
            /// </summary>
            /// <returns>A copy of this characteristic</returns>
            public virtual object Clone()
            {
                CharacteristicBase outClone = new CharacteristicBase();
                outClone.name = name;
                outClone.comment = comment;
                outClone.dataType = dataType;
                outClone.required = required;
                outClone.boolValue = boolValue;
                outClone.intValue = intValue;
                outClone.floatValue = floatValue;
                outClone.stringValue = stringValue;

                return outClone;
            }
        }
    }
}
