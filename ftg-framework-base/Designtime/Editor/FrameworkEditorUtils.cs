﻿using FlamingTorchGames.CommonScripts;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace FlamingTorchGames.FrameworkBase
{
    public class FrameworkEditorUtils
    {
        public delegate void ShowCharacteristicElementDelegate(ref Rect rect, SerializedProperty element, int dataType);

        /// <summary>
        /// Creates a characteristic editor for a reorderable list
        /// </summary>
        /// <param name="list">The list to modify</param>
        /// <param name="characteristicHandler">The characteristic handler for non-standard characteristics (will be called if the data type is non-standard)</param>
        /// <param name="dataTypeNames">The names of the data types to display</param>
        /// <param name="editingValueOnly">Whether we are not editing names and types</param>
        /// <param name="hideValues">Whether to hide values from visibility/editing</param>
        public static void CharacteristicReorderedListEditor(ReorderableList list, ShowCharacteristicElementDelegate characteristicHandler, string[] dataTypeNames, bool editingValueOnly, bool hideValues = false)
        {
            list.drawElementCallback += (Rect rect, int index, bool isActive, bool isFocused) =>
            {
                var element = list.serializedProperty.GetArrayElementAtIndex(index);

                if (element == null)
                {
                    return;
                }

                var nameProperty = element.FindPropertyRelative("name");
                var commentProperty = element.FindPropertyRelative("comment");
                var dataTypeProperty = element.FindPropertyRelative("dataType");
                var nameWidth = 100.0f;

                if (!editingValueOnly)
                {
                    for (int i = 0; i < list.serializedProperty.arraySize; i++)
                    {
                        var listElement = list.serializedProperty.GetArrayElementAtIndex(i);

                        if (listElement != null)
                        {
                            var listElementNameProperty = listElement.FindPropertyRelative("name");

                            if (listElementNameProperty != null && listElementNameProperty.propertyType == SerializedPropertyType.String)
                            {
                                nameWidth = Mathf.Max(nameWidth, EditorDesignerUtils.LabelWidth(listElementNameProperty.stringValue) + 10);
                            }
                        }
                    }
                }

                if (nameProperty != null && nameProperty.propertyType == SerializedPropertyType.String)
                {
                    if (editingValueOnly)
                    {
                        EditorDesignerUtils.Label(ref rect, nameProperty.stringValue, nameWidth);
                    }
                    else
                    {
                        EditorDesignerUtils.PrefixLabel(ref rect, "Name");
                        nameProperty.stringValue = EditorDesignerUtils.TextField(ref rect, nameProperty.stringValue, nameWidth);
                    }
                }

                if (!editingValueOnly && commentProperty != null && commentProperty.propertyType == SerializedPropertyType.String)
                {
                    EditorDesignerUtils.PrefixLabel(ref rect, "Comment");
                    commentProperty.stringValue = EditorDesignerUtils.TextField(ref rect, commentProperty.stringValue, 60);
                }

                if (dataTypeProperty != null && dataTypeProperty.propertyType == SerializedPropertyType.Integer)
                {
                    var width = 0.0f;

                    if (!editingValueOnly)
                    {
                        EditorDesignerUtils.PrefixLabel(ref rect, "Type");

                        dataTypeProperty.intValue = (int)EditorDesignerUtils.Popup(ref rect, dataTypeNames.ToList(), dataTypeProperty.intValue);
                    }

                    if (hideValues || dataTypeProperty.intValue < 0 || dataTypeProperty.intValue >= dataTypeNames.Length)
                    {
                        return;
                    }

                    if (!editingValueOnly)
                    {
                        EditorDesignerUtils.PrefixLabel(ref rect, "Value");
                    }

                    SerializedProperty valueProperty = null;

                    switch(dataTypeNames[dataTypeProperty.intValue])
                    {
                        case CharacteristicBase.BoolDataTypeName:
                            valueProperty = element.FindPropertyRelative("boolValue");

                            if (valueProperty != null)
                            {
                                width = 60;
                                valueProperty.boolValue = EditorDesignerUtils.Toggle(ref rect, valueProperty.boolValue, width);
                            }

                            break;

                        case CharacteristicBase.FloatDataTypeName:
                            valueProperty = element.FindPropertyRelative("floatValue");

                            if (valueProperty != null)
                            {
                                width = 60;
                                valueProperty.floatValue = EditorDesignerUtils.FloatField(ref rect, valueProperty.floatValue, width);
                            }

                            break;

                        case CharacteristicBase.IntDataTypeName:
                            valueProperty = element.FindPropertyRelative("intValue");

                            if (valueProperty != null)
                            {
                                width = 60;
                                valueProperty.intValue = EditorDesignerUtils.IntField(ref rect, valueProperty.intValue, width);
                            }

                            break;

                        case CharacteristicBase.StringDataTypeName:
                            valueProperty = element.FindPropertyRelative("stringValue");

                            if (valueProperty != null)
                            {
                                width = 60;
                                valueProperty.stringValue = EditorDesignerUtils.TextArea(ref rect, valueProperty.stringValue, width);
                            }

                            break;

                        default:
                            if(characteristicHandler != null)
                            {
                                characteristicHandler(ref rect, element, dataTypeProperty.intValue);
                            }

                            break;
                    }
                }
            };
        }
    }
}
