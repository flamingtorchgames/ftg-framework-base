﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System;

namespace FlamingTorchGames
{
    namespace FrameworkBase
    {
        public class FrameworkMainWindowBase<WorkspaceManagerType, GameComponentsType> : EditorWindow
            where WorkspaceManagerType : WorkspaceManagerBase
            where GameComponentsType : GameComponentsInfoBase
        {
            protected List<string> componentTabTitles = new List<string>();
            protected List<ScriptableObject> componentInstances = new List<ScriptableObject>();
            protected List<Editor> componentEditors = new List<Editor>();

            protected int tabTitlesPerToolbar = 7;
            protected int currentTab = 0;
            protected int currentGame = -1;
            protected string newGameName = "";
            protected bool needsEditorUpdate = true;
            protected Vector2 scrollPosition = Vector2.zero;
            protected WorkspaceManagerType workspaceInstance;

            protected virtual void GetComponentDetails()
            {
                componentInstances = new List<ScriptableObject>();
                componentEditors = new List<Editor>();
                componentTabTitles = new List<string>();
            }

            protected void AddWorkspaceComponent(ScriptableObject component, string tabTitle)
            {
                componentInstances.Add(component);
                componentEditors.Add(EditorForComponent(component));
                componentTabTitles.Add(tabTitle);
            }

            protected Editor EditorForComponent(ScriptableObject component)
            {
                return Editor.CreateEditor(component);
            }

            protected virtual void OnBeginGUI()
            {
            }

            protected virtual void OnEndGUI()
            {
            }

            private void OnGUI()
            {
                OnBeginGUI();

                if (workspaceInstance == null)
                {
                    OnEndGUI();

                    return;
                }

                scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);
                List<string> gamesList = workspaceInstance.GetGamesList();
                string[] displayGamesList = null;

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.PrefixLabel("Name");
                newGameName = EditorGUILayout.TextField(newGameName);
                EditorGUILayout.EndHorizontal();

                if (GUILayout.Button("Create New") && newGameName.Length > 0)
                {
                    if (!workspaceInstance.IsGameWorkspaceValid(newGameName) && workspaceInstance.CreateGameInWorkspace(newGameName))
                    {
                        gamesList = workspaceInstance.GetGamesList();

                        currentGame = gamesList.IndexOf(newGameName);
                        needsEditorUpdate = true;

                        workspaceInstance.CurrentlyEditedGame = newGameName;
                    }
                }

                displayGamesList = gamesList.Select((x, xIndex) => (xIndex + 1) + " " + x).ToArray();

                int previousGame = currentGame;

                if (workspaceInstance.CurrentlyEditedGame != null)
                {
                    currentGame = gamesList.IndexOf(workspaceInstance.CurrentlyEditedGame);
                }

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.PrefixLabel("Current Game: ");
                currentGame = EditorGUILayout.Popup(currentGame, displayGamesList);
                EditorGUILayout.EndHorizontal();

                needsEditorUpdate = needsEditorUpdate || (previousGame != currentGame);

                if (currentGame < 0 && gamesList.Count > 0)
                {
                    currentGame = 0;
                }

                if (currentGame >= 0 && currentGame < gamesList.Count)
                {
                    string currentGameName = gamesList[currentGame];
                    workspaceInstance.CurrentlyEditedGame = currentGameName;

                    if (needsEditorUpdate)
                    {
                        needsEditorUpdate = false;
                        workspaceInstance.CurrentlyEditedGame = currentGameName;

                        GetComponentDetails();
                    }

                    if (componentInstances.Count != componentEditors.Count)
                    {
                        OnEndGUI();

                        return;
                    }

                    var tabTitles = componentTabTitles.Concat(new string[] { "Import/Export" }).ToList();

                    for (int i = 0; i < Mathf.Max(tabTitles.Count / tabTitlesPerToolbar, 1); i++)
                    {
                        var titles = tabTitles.Skip(tabTitlesPerToolbar * i).ToArray();
                        var index = currentTab - tabTitlesPerToolbar * i;

                        var selectedTab = GUILayout.Toolbar(index, titles);

                        if(selectedTab >= 0 && selectedTab < tabTitlesPerToolbar)
                        {
                            currentTab = i * tabTitlesPerToolbar + selectedTab;
                        }
                    }

                    if (currentTab >= 0 && currentTab < componentEditors.Count)
                    {
                        if (componentInstances[currentTab] != null && componentEditors[currentTab] != null)
                        {
                            componentEditors[currentTab].OnInspectorGUI();

                            if (GUI.changed)
                            {
                                EditorUtility.SetDirty(componentInstances[currentTab]);
                            }
                        }
                    }
                    else if (currentTab == componentEditors.Count)
                    {
                        var gamePath = workspaceInstance.GameWorkspaceDirectory(currentGameName);

                        EditorGUILayout.BeginHorizontal();

                        if (GUILayout.Button("Import"))
                        {
                            try
                            {
                                var path = EditorUtility.OpenFilePanelWithFilters("Import JSON data", gamePath,
                                    new string[] { "JSON Files", "json", "All Files", "*" });

                                if (path != null && path.Length > 0)
                                {
                                    var importedGameDataString = System.IO.File.ReadAllText(path);
                                    var gameComponents = (GameComponentsType)Activator.CreateInstance(typeof(GameComponentsType), workspaceInstance, currentGameName);

                                    if (gameComponents == null || !gameComponents.Import(importedGameDataString))
                                    {
                                        throw new Exception();
                                    }

                                    EditorUtility.DisplayDialog("Import succeeded", "Successfully imported", "OK");
                                }
                            }
                            catch (Exception)
                            {
                                EditorUtility.DisplayDialog("Import failed", "There was an error while attempting to import", "OK");
                            }
                        }

                        if (GUILayout.Button("Export"))
                        {
                            try
                            {
                                var path = EditorUtility.SaveFilePanel("Save JSON data", gamePath, "GameData", "json");
                                var gameComponents = (GameComponentsType)Activator.CreateInstance(typeof(GameComponentsType), workspaceInstance, currentGameName);

                                if(gameComponents == null)
                                {
                                    throw new Exception();
                                }

                                if (path != null && path.Length > 0)
                                {
                                    var exportedGameDataString = gameComponents.Export();

                                    System.IO.File.WriteAllText(path, exportedGameDataString);

                                    EditorUtility.DisplayDialog("Export succeeded", "Successfully exported", "OK");
                                }
                            }
                            catch (Exception)
                            {
                                EditorUtility.DisplayDialog("Export failed", "There was an error while attempting to export", "OK");
                            }
                        }

                        EditorGUILayout.EndHorizontal();
                    }
                }

                EditorGUILayout.EndScrollView();

                OnEndGUI();
            }
        }
    }
}
